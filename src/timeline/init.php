<?php
/**
 * Timeline Block Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 * Also register rest route and initialize Gutenberg block.
 *
 * @since   1.0.0
 * @package gutenberg-extra-block
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class Timeline_Block {
    /**
     * Initialize actions for Gutenberg.
     *
     * @since 2.7.0
     */
    public function __construct() {
        add_action( 'init', array( $this, 'gutenberg_extra_block_assets' ) );
    }

    /**
     * Enqueue Gutenberg block assets for both frontend + backend.
     *
     * Assets enqueued:
     * 1. blocks.style.build.css - Frontend + Backend.
     * 2. blocks.build.js - Backend.
     * 3. blocks.editor.build.css - Backend.
     *
     * @uses {wp-blocks} for block type registration & related functions.
     * @uses {wp-element} for WP Element abstraction — structure of blocks.
     * @uses {wp-i18n} to internationalize the block's text.
     * @uses {wp-editor} for WP editor styles.
     * @since 1.0.0
     */
    function gutenberg_extra_block_assets() {
        /**
         * Register Gutenberg block on server-side.
         *
         * Register the block on server-side to ensure that the block
         * scripts and styles for both frontend and backend are
         * enqueued when the editor loads.
         *
         * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
         * @since 1.16.0
         */
        register_block_type(
            'geb/block-timeline', array(
                // Enqueue blocks.style.build.css on both frontend & backend.
                'style'         => 'gutenberg-extra-blocks-style-css',
                // Enqueue blocks.build.js in the editor only.
                'editor_script' => 'gutenberg-extra-blocks-js',
                // Enqueue blocks.editor.build.css in the editor only.
                'editor_style'  => 'gutenberg-extra-blocks-editor-css',
                // Render callback
                'render_callback' => array( $this, 'block_timeline_frontend' ),
            )
        );
    } // Hook: __construct.

    /**
     * Block front-end output.
     *
     * @since 2.7.0
     * @see register_block
     *
     * @param array $attributes Array of passed shortcode attributes.
     * @return ob_cache HTML canvas.
     */
    public function block_timeline_frontend( $attributes ) {
        if ( is_admin() || defined( 'REST_REQUEST' ) ) {
            return;
        }
        if ( is_array( $attributes ) && array_key_exists( 'items', $attributes ) ) {
            $items = $attributes['items'];
            $timeline_id = rand(1,10);
            ob_start();
            ?>
            <div id="timeline-<?php esc_attr_e( $timeline_id ); ?>">
                <?php $running_state = false; ?>
                <?php foreach( $items as $item) : ?>
                    <?php
                    $event_id       = $item[1];
                    $event_DateTime = new DateTime( $event_id );
                    $is_passed      = new DateTime( 'now' ) > $event_DateTime ? true : false;
                    if ( new DateTime( 'now' ) <= $event_DateTime && !$running_state && $running_state !== null ) {
                        $running_state = true;
                    }
                    $event_date     = $event_DateTime->format( get_option('date_format') );
                    $event_title    = $item[0];
                    $event_content  = $item[2];
                    ?>
                    <div id=<?php esc_attr_e( $event_id ); ?> class="event <?php $is_passed ? esc_attr_e( 'accomplished' ) : '' ?> <?php $running_state ? esc_attr_e( 'running' ) : '' ?>">
                        <span class="event-date"><?php esc_html_e( $event_date ); ?></span>
                        <span class="event-title"><?php esc_html_e( $event_title); ?></span>
                        <p class="event-content"><?php esc_html_e( $event_content ); ?></p>
                        <?php echo $running_state ? '<span class="rocket-icon"></span>' : ''; ?>
                        <?php if ( $running_state ) {
                            $running_state = null;
                        }   ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php
            return ob_get_clean();
        }
    } // Callback: gutenberg_extra_block_assets.

} new Timeline_Block();