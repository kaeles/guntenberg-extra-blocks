import Slider from 'react-slick';

const { Component, Fragment } = wp.element;
const { __, _x } = wp.i18n;
const {
    Panel,
    PanelBody,
    SelectControl,
    TextControl,
    TextareaControl,
    Toolbar,
    ToggleControl,
    Button,
    ButtonGroup,
    PanelRow,
    ExternalLink,
} = wp.components;
const {
    InspectorControls,
    BlockControls,
    AlignmentToolbar
} = wp.editor;
const {
    RichText
} = wp.blockEditor;
const el = wp.element.createElement;

class Timeline extends Component {

    constructor() {

        super( ...arguments );

        this.state = {
            items: this.props.attributes.items || [''],

        };
        this.props.attributes.items     = this.state.items;
        this.props.attributes.title     = this.state.title;
        this.props.attributes.alignment = this.state.alignment;

    };

    /**
     * Add a new slide.
     */
    addClick = () => {
        this.setState(prevState => ({ items: [...prevState.items, '']}));
    };

    /**
     * Remove a Slide.
     */
    removeClick = () => {
        let items = [...this.props.attributes.items];
        items.splice(items,1);
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    /**
     * Update the label when it's changed.
     */
    handleLabelChange = (i, val) => {
        if ( undefined == val ) {
            return;
        }
        let items = [...this.state.items];
        if ( items.length == 0 ) {
            return;
        }
        items[i] = [ val || '', items[i][1] || '', items[i][2] || '' ];
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    /**
     * Update the date when it's changed.
     */
    handleDateChange = (i, val) => {
        if ( undefined == val ) {
            return;
        }
        let items = [...this.state.items];
        if ( items.length == 0 ) {
            return;
        }
        items[i] = [ items[i][0] || '', val, items[i][2] || '' ];
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    /**
     * Update the date when it's changed.
     */
    handleContentChange = (i, val) => {
        if ( undefined == val ) {
            return;
        }
        let items = [...this.state.items];
        if ( items.length == 0 ) {
            return;
        }
        items[i] = [ items[i][0] || '', items[i][1] || '', val ];
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    showItems = () => {
        return ( this.state.items.map((element, i) =>
                el(PanelRow, {},
                    el('div', {className: 'item'},
                        el(TextControl,
                            {
                                label: __('Label'),
                                className: 'label',
                                value: undefined !== this.props.attributes.items[i] ? this.props.attributes.items[i][0] : '',
                                autoFocus: true,
                                onChange: this.handleLabelChange.bind(this, i)
                            }
                        ),
                        el(TextControl,
                            {
                                label: __('Date'),
                                type: 'date',
                                value: undefined !== this.props.attributes.items[i] ? this.props.attributes.items[i][1] : '',
                                onChange: this.handleDateChange.bind(this, i)
                            }
                        ),
                        el(TextareaControl,
                            {
                                label: __('Content'),
                                className: 'content',
                                value: undefined !== this.props.attributes.items[i] ? this.props.attributes.items[i][2] : '',
                                onChange: this.handleContentChange.bind(this, i)
                            }
                        ),
                    ),
                ),
            )
        ) };

    renderBlock = () => {
        const items = this.props.attributes.items;
        const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
        const userLang = navigator.language;
        const settings = {
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
        };
        return el( 'div', {},
            <Slider {...settings}>
                {items.map((item) => (
                    <div id={item[1]} className="event">
                        <span className="event-date">{new Date(item[1]).toLocaleDateString(userLang, options)}</span>
                        <span className="event-title">{item[0]}</span>
                        <p className="event-content">{item[2]}</p>
                    </div>
                ))}
            </Slider>
        );
    };

    render() {
        return (
            el( 'div', { className: this.props.className },
                this.renderBlock(),
                el( InspectorControls, {},
                    el( PanelBody, {title: __( 'Timeline data' ) },
                        el('div', {},
                            this.showItems()
                        ),
                        el( ButtonGroup, {},
                            el( Button, {
                                    className: 'button add-row',
                                    onClick: this.addClick
                                },
                                __('Add Row')
                            ),
                            el( Button, {
                                    className: 'button remove-row',
                                    isDestructive: true,
                                    onClick: this.removeClick
                                },
                                __('Reset')
                            )
                        ),
                    ),
                )
            )
        )
    }
}

export default Timeline;