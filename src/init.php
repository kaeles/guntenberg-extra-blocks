<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

require SRC_DIR .  'functions.php';

// Exit if Gutenberg is not active.
if ( ! geb_gutenberg_is_active() ) {
    exit;
}

require SRC_DIR . 'chart-pie/init.php';
require SRC_DIR . 'timeline/init.php';

function gutenberg_extra_block_assets() {
    // Register block styles for both frontend.
    wp_register_style(
        'gutenberg-extra-blocks-style-css', // Handle.
        DIST_URL . 'blocks.style.build.css', // Block style CSS.
        array( 'wp-editor' ), // Dependency to include the CSS after it.
        filemtime( DIST_DIR . 'blocks.style.build.css' ) // Version: File modification time.
    );

    // Register block editor styles for backend.
    wp_register_style(
        'gutenberg-extra-blocks-editor-css',
        DIST_URL . 'blocks.editor.build.css',
        array( 'wp-edit-blocks' ),
        filemtime( DIST_DIR . 'blocks.editor.build.css' )
    );

    // Register block editor script for backend.
    wp_register_script(
        'gutenberg-extra-blocks-js',
        DIST_URL . 'blocks.build.js',
        array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
        filemtime( DIST_DIR . 'blocks.build.js' ),
        true // Enqueue the script in the footer.
    );
}
add_action( 'init', 'gutenberg_extra_block_assets' );

function gutenberg_extra_block_front_assets() {
    // Register block editor script for frontend.
    wp_enqueue_script(
        'chartjs', // Handle.
        '//cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js',
        array(), // Dependencies, defined above.
        null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
        true // Enqueue the script in the footer.
    );
    // Register block editor script for frontend.
    wp_enqueue_script(
        'chartkick-js', // Handle.
        '//unpkg.com/chartkick@3.1.1/dist/chartkick.js',
        array('chartjs'), // Dependencies, defined above.
        null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
        true // Enqueue the script in the footer.
    );
    // Register Slick carousel script for frontend.
    wp_enqueue_script(
        'slick-carousel',
        '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js',
        null,
        null,
        true
    );
    // Register block editor script for frontend.
    wp_enqueue_script(
        'gutenberg-extra-blocks-fronts-js', // Handle.
        plugins_url( '/dist/fronts.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
        array('chartkick-js'), // Dependencies, defined above.
        null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
        true // Enqueue the script in the footer.
    );
}
add_action( 'wp_enqueue_scripts', 'gutenberg_extra_block_front_assets' );