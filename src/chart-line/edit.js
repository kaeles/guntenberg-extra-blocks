import {LineChart} from "react-chartkick";

const { Component, Fragment } = wp.element;

const { __, _x } = wp.i18n;

const {
    Panel,
    PanelBody,
    SelectControl,
    TextControl,
    TextareaControl,
    Toolbar,
    ToggleControl,
    Button,
    ButtonGroup,
    PanelRow,
    ExternalLink,
} = wp.components;

const {
    InspectorControls,
    BlockControls,
    AlignmentToolbar
} = wp.editor;

const {
    RichText
} = wp.blockEditor;

const el = wp.element.createElement;


class Line_Pie extends Component {

    constructor() {

        super( ...arguments );

        this.state = {
            items: this.props.attributes.items || [''],

        };
        this.props.attributes.items     = this.state.items;
        this.props.attributes.title     = this.state.title;
        this.props.attributes.alignment = this.state.alignment;

    };

    /**
     * Add a new slide.
     */
    addClick = () => {
        this.setState(prevState => ({ items: [...prevState.items, '']}));
    };

    /**
     * Remove a Slide.
     */
    removeClick = () => {
        let items = [...this.props.attributes.items];
        items.splice(items,1);
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    /**
     * Update the label when it's changed.
     */
    handlexChange = (i, val) => {
        if ( undefined == val ) {
            return;
        }
        let items = [...this.state.items];
        if ( items.length == 0 ) {
            return;
        }
        //items{i} : { Object.keys( this.props.attributes.items[i] ) || '': items[i] || '' };
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    /**
     * Update the value when it's changed.
     */
    handleyChange = (i, val) => {
        if ( undefined == val ) {
            return;
        }
        let items = [...this.state.items];
        if ( items.length == 0 ) {
            return;
        }
        if (val > 100) {
            val = 100;
        }
        items[i] = [ items[i][0] || '', val ];
        this.props.setAttributes( { items: items } );
        this.setState({ items });
    };

    showItems = () => {
        return ( this.state.items.map((element, i) =>
                el(PanelRow, {},
                    el('div', {className: 'item'},
                        el(TextControl,
                            {
                                label: __('x value'),
                                className: 'x-value',
                                value: undefined !== this.props.attributes.items[i] ? this.props.attributes.items[i] : '',
                                autoFocus: true,
                                onChange: this.handleChange.bind(this, i)
                            }
                        ),
                        el(TextControl,
                            {
                                label: __('y value'),
                                className: 'y-value',
                                value: undefined !== this.props.attributes.items[i] ? Object.keys( this.props.attributes.items[i] ) : '',
                                onChange: this.handleChange.bind(this, i)
                            }
                        ),
                    ),
                ),
            )
        ) };

    render() {
        return el(
            'div',
            { className: this.props.className },
            el(
                'div',
                { className: 'item-list' },
                el( InspectorControls, {},
                    el( PanelBody, {title: __( 'Chart data' ) },
                        el('div', {className: 'item-list'},
                            this.showItems()
                        ),
                        el( ButtonGroup, {},
                            el( Button, {
                                    className: 'button add-row',
                                    onClick: this.addClick
                                },
                                __('Add Row')
                            ),
                            el( Button, {
                                    className: 'button remove-row',
                                    isDestructive: true,
                                    onClick: this.removeClick
                                },
                                __('Reset')
                            )
                        ),
                    ),
                ),
            ),
            el( 'div', {},
                <LineChart data={this.props.attributes.items}/>
            )
        )
    }
}

export default Line_Pie;