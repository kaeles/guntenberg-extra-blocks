<?php
/**
 * Chart Pie Block Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 * Also register rest route and initialize Gutenberg block.
 *
 * @since   1.0.0
 * @package gutenberg-extra-block
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class Chart_Pie_Block {
    /**
     * Initialize actions for Gutenberg.
     *
     * @since 2.7.0
     */
    public function __construct() {
        add_action( 'init', array( $this, 'gutenberg_extra_block_assets' ) );
    }

    /**
     * Enqueue Gutenberg block assets for both frontend + backend.
     *
     * Assets enqueued:
     * 1. blocks.style.build.css - Frontend + Backend.
     * 2. blocks.build.js - Backend.
     * 3. blocks.editor.build.css - Backend.
     *
     * @uses {wp-blocks} for block type registration & related functions.
     * @uses {wp-element} for WP Element abstraction — structure of blocks.
     * @uses {wp-i18n} to internationalize the block's text.
     * @uses {wp-editor} for WP editor styles.
     * @since 1.0.0
     */
    function gutenberg_extra_block_assets() {
        /**
         * Register Gutenberg block on server-side.
         *
         * Register the block on server-side to ensure that the block
         * scripts and styles for both frontend and backend are
         * enqueued when the editor loads.
         *
         * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
         * @since 1.16.0
         */
        register_block_type(
            'geb/block-chart-pie', array(
                // Enqueue blocks.style.build.css on both frontend & backend.
                'style'         => 'gutenberg-extra-blocks-style-css',
                // Enqueue blocks.build.js in the editor only.
                'editor_script' => 'gutenberg-extra-blocks-js',
                // Enqueue blocks.editor.build.css in the editor only.
                'editor_style'  => 'gutenberg-extra-blocks-editor-css',
                // Render callback
                'render_callback' => array( $this, 'block_chart_pie_frontend' ),
            )
        );
    } // Hook: __construct.

    /**
     * Block front-end output.
     *
     * @since 2.7.0
     * @see register_block
     *
     * @param array $attributes Array of passed shortcode attributes.
     * @return ob_cache HTML canvas.
     */
    public function block_chart_pie_frontend( $attributes ) {
        if ( is_admin() || defined( 'REST_REQUEST' ) ) {
            return;
        }
        if ( is_array( $attributes ) && array_key_exists( 'items', $attributes ) ) {
            $items = $attributes['items'];
            $chart_id = rand(1,10);
            $chart_pie = array(
                $chart_id => $items
            );
            ob_start();
            ?>
            <div class="geb-chart-pie-block">
                <div id="geb-chart-<?php esc_attr_e( $chart_id ) ?>"></div>
            </div>
            <?php
            wp_localize_script( 'gutenberg-extra-blocks-fronts-js', 'chartBlocks', json_encode( $chart_pie ) );
            return ob_get_clean();
        }
    } // Callback: gutenberg_extra_block_assets.

} new Chart_Pie_Block();