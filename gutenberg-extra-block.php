<?php
/**
 * Plugin Name: Gutenberg Extra Blocks
 * Plugin URI: https://digital-wheel.com/wordpress-plugins/gutenberg-extra-blocks/
 * Description: Gutenberg Extra Blocks — provides some extra blocks for Gutenberg editor. Powered by "Create guten block"
 * Author: Kaeles
 * Author URI: https://digital-wheel.com/kaeles
 * Version: 0.1
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * CGB github: https://github.com/ahmadawais/create-guten-block
 *
 * @package GEB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'SRC_DIR', plugin_dir_path( __FILE__ ) . 'src/' );
define( 'SRC_URL', plugin_dir_url( __FILE__ ) . 'src/' );
define( 'DIST_DIR', plugin_dir_path( __FILE__ ) . 'dist/' );
define( 'DIST_URL', plugin_dir_url( __FILE__ ) . 'dist/' );

/**
 * Block Initializer.
 */
function init_blocks() {
    require_once SRC_DIR . 'init.php';
}
add_action( 'plugins_loaded', 'init_blocks' );
